$(function () {
    var APPLICATION_ID = "4071B0A9-00D1-43D6-FF58-EFBC29193C00",
        SECRET_KEY = "F864412D-4FC4-22B3-FF2E-2748FA1F7300",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    
    
    var postsCollection = Backendless.Persistence.of(Posts).find();
    
    console.log(postsCollection);
    
    var wrapper = {
         posts: postsCollection.data   
     };
     
     Handlebars.registerHelper('format', function (time) {
         return moment(time).format("dddd, MMMM Do YYYY");
     });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
    
    $('.button-collapse').sideNav('show');
    $('.button-collapse').sideNav('hide');
});


function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

